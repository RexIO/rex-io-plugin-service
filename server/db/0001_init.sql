DROP TABLE IF EXISTS `service_type`;
CREATE TABLE `service_type` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `name` varchar(255) NOT NULL,
     PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET utf8 ;

DROP TABLE IF EXISTS `service`;
CREATE TABLE `service` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `name` varchar(255) DEFAULT NULL,
     PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET utf8 ;

DROP TABLE IF EXISTS `hardware_service`;
CREATE TABLE `hardware_service` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `hardware_id` int(11) DEFAULT NULL,
     `service_id` int(11) DEFAULT NULL,
     PRIMARY KEY (`id`),
     KEY `hardware_id` (`hardware_id`)
) ENGINE=InnoDB CHARACTER SET utf8 ;

ALTER TABLE service ADD COLUMN (`type_id` int(11) NOT NULL DEFAULT 1);
ALTER TABLE service ADD KEY `type_id` (`type_id`);
INSERT INTO service_type (id, name) VALUES (1,"rex"),(2,"puppet");
