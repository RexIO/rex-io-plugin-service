package Rex::IO::Server::Service;
use Mojo::Base 'Mojolicious::Controller';

use Cwd qw(getcwd);
use Mojo::JSON "j";
use Data::Dumper;
use Try::Tiny;
use Carp;

sub register_service {
  my ($self) = @_;

  try {
    my $json = $self->req->json;
    $self->app->log->debug( "Creating new service:\n" . Dumper($json) );
    my $itm = $self->db->resultset("Service")->create($json);
    $self->render(
      json => { ok => Mojo::JSON->true, data => $itm->to_hashRef } );
    1;
  }
  catch {
    $self->app->log->error("Can't create new service:\n\nERROR: $_\n\n");
    $self->render(
      json   => { ok => Mojo::JSON->false, error => $_ },
      status => 500
    );
  };
}

sub remove_service {
  my ($self) = @_;
  my $id = $self->param("service_id");

  try {
    $self->app->log->debug("Deleting service: $id");

    my $hardware_service = $self->db->resultset("HardwareService")->find(
      {
        service_id => $id
      }
    );

    confess "Service is still registered to a host. Please remove first."
      if $hardware_service;

    my $itm = $self->db->resultset("Service")->find($id);
    if ( !$itm ) {
      confess "Can't find service with id: $id";
    }
    $itm->delete;
    $self->render( json => { ok => Mojo::JSON->true } );
    1;
  }
  catch {
    $self->app->log->error("Can't delete service $id:\n\nERROR: $_\n\n");
    $self->render(
      json   => { ok => Mojo::JSON->false, error => $_ },
      status => 500
    );
  };

}

sub add_service_to_host {
  my ($self)     = @_;
  my $service_id = $self->param("service_id");
  my $host_id    = $self->param("host_id");

  try {
    $self->app->log->debug("Adding service $service_id to hardware $host_id");

    my $service_itm = $self->db->resultset("Service")->find($service_id);
    my $host_itm    = $self->db->resultset("Hardware")->find($host_id);

    confess "No service with service_id: $service_id found."
      unless $service_itm;
    confess "No hardware with hardware_id: $host_id found."
      unless $host_itm;

    my $hs_itm = $self->db->resultset("HardwareService")->create(
      {
        hardware_id => $host_itm->id,
        service_id  => $service_itm->id,
      }
    );

    $self->render(
      json => { ok => Mojo::JSON->true, data => $hs_itm->to_hashRef } );
    1;
  }
  catch {
    $self->app->log->error("Error adding service to host:\n\n$_\n\n");
    $self->render(
      json   => { ok => Mojo::JSON->false, error => $_ },
      status => 500
    );
  };

}

sub remove_service_from_host {
  my ($self)      = @_;
  my $service_id  = $self->param("service_id");
  my $hardware_id = $self->param("host_id");

  try {
    $self->app->log->debug(
      "Removing service $service_id from hardware $hardware_id");

    my $itm = $self->db->resultset("HardwareService")->search(
      {
        service_id  => $service_id,
        hardware_id => $hardware_id,
      }
    );

    $itm->delete;

    $self->render( json => { ok => Mojo::JSON->true } );
    1;
  }
  catch {
    $self->app->log->error(
      "Error removing service from hardware:\n\nERROR: $_\n\n");
    $self->render(
      json   => { ok => Mojo::JSON->false, error => $_ },
      status => 500
    );
  };

}

sub remove_all_services_from_host {
  my ($self) = @_;
  my $hardware_id = $self->param("host_id");

  try {
    $self->app->log->debug("Removing all services from hardware $hardware_id");

    my $itm = $self->db->resultset("HardwareService")->search(
      {
        hardware_id => $hardware_id,
      }
    );

    $itm->delete;

    $self->render( json => { ok => Mojo::JSON->true } );
    1;
  }
  catch {
    $self->app->log->error(
      "Error removing services from hardware:\n\nERROR: $_\n\n");
    $self->render(
      json   => { ok => Mojo::JSON->false, error => $_ },
      status => 500
    );
  };

}

sub get_services {
  my ($self) = @_;

  my @ret;

  my @services = $self->db->resultset("Service")->search;
  for my $service (@services) {
    push @ret, $service->to_hashRef;
  }

  $self->render( json => { ok => Mojo::JSON->true, data => \@ret } );
}

sub get_services_from_host {
  my ($self) = @_;

  my @ret;

  my @services = $self->db->resultset("HardwareService")->search(
    {
      hardware_id => $self->param("host_id")
    }
  );

  for my $service (@services) {
    push @ret, $service->service->to_hashRef;
  }

  $self->render( json => { ok => Mojo::JSON->true, data => \@ret } );
}

sub delete_services {
  my ($self) = @_;

  my $param_str = $self->param("search");

  try {
    if ( !$param_str ) {
      confess "Try to delete services without a search paramter.";
    }

    my $p      = Mojo::Parameters->new($param_str);
    my %params = @{ $p->params };

    my $services = $self->db->resultset("Service")->search( \%params );
    $services->delete;
    1;
  }
  catch {
    $self->app->log->error($_);
    $self->render( json => { ok => Mojo::JSON->false, error => $_ } );
  };

}

sub run_service_on_host {
  my ($self) = @_;
  $self->render( json => { ok => Mojo::JSON->true } );
}

sub __register__ {
  my ( $self, $app ) = @_;
  my $r = $app->routes;

  # register new service
  $r->post("/1.0/service/service")->over( authenticated => 1 )
    ->to("service#register_service");

  # remove a service
  $r->delete("/1.0/service/service/:service_id")->over( authenticated => 1 )
    ->to("service#remove_service");

  # add a service to a host
  $r->post("/1.0/service/service/:service_id/host/:host_id")
    ->over( authenticated => 1 )->to("service#add_service_to_host");

  # delete a service from a host
  $r->delete("/1.0/service/service/:service_id/host/:host_id")
    ->over( authenticated => 1 )->to("service#remove_service_from_host");

  # delete all services from a host
  $r->delete("/1.0/service/host/:host_id")->over( authenticated => 1 )
    ->to("service#remove_all_services_from_host");

  # run a service on a host
  $r->post("/1.0/service/service/:service_id/host/:host_id/run")
    ->over( authenticated => 1 )->to("service#run_service_on_host");

  # get all services from a host
  $r->get("/1.0/service/host/:host_id")->over( authenticated => 1 )
    ->to("service#get_services_from_host");

  # get all services
  $r->get("/1.0/service/service")->over( authenticated => 1 )
    ->to("service#get_services");

  # delete services with a search query
  $r->delete("/1.0/service/service")->over( authenticated => 1 )
    ->to("service#delete_services");

#  $r->route("/1.0/service")->via("RUN")->to("service#run_tasks");
#  $r->route("/1.0/service")->via("LIST")->to("service#get_all");
#  $r->route("/1.0/service/:id")->via("LIST")->to("service#get_service");
#  $r->route("/1.0/service/host/:hostid")->via("LIST")->to("service#get_service_for_host");
#  $r->delete("/1.0/service/host/:hostid")->to("service#remove_all_tasks_from_host");
}

1;
