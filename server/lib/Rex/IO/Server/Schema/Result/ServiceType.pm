# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:

package Rex::IO::Server::Schema::Result::ServiceType;

use strict;
use warnings;

use base qw(DBIx::Class::Core);

__PACKAGE__->table("service_type");
__PACKAGE__->add_columns(qw/id name/);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->has_many( "services", "Rex::IO::Server::Schema::Result::Service",
  "type_id" );


sub to_hashRef {
  my $self = shift;
  return { $self->get_columns };
}

1;
