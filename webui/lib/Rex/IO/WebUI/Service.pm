package Rex::IO::WebUI::Service;

use Mojo::Base 'Mojolicious::Controller';
use Data::Dumper;
use File::Basename;

sub register_services_to_server {
  my ($self) = @_;

  my $server_id = $self->param("server_id");
  my $json      = $self->req->json;

  # first remove all current registered services
  $self->app->log->debug("removing all services from server_id: $server_id.");
  $self->rexio->call( "DELETE", "1.0", "service",
    host => $self->param("server_id") );

  # then register the new one
  my @ret;
  for my $service ( @{ $json->{data} } ) {
    $self->app->log->debug(
      "registering service_id: $service on server: $server_id");
    push @ret,
      $self->rexio->call(
      "POST", "1.0", "service",
      service => $service,
      host    => $server_id
      );
  }

  $self->render( json => { ok => Mojo::JSON->true, data => \@ret } );
}

##### Rex.IO WebUI Plugin specific methods
sub __register__ {
  my ( $self, $opt ) = @_;
  my $r      = $opt->{route};
  my $r_auth = $opt->{route_auth};
  my $app    = $opt->{app};

  $r_auth->post("/1.0/service/server/:server_id")
    ->to("service#register_services_to_server");

  # add plugin template path
  push( @{ $app->renderer->paths }, dirname(__FILE__) . "/templates" );
  push( @{ $app->static->paths },   dirname(__FILE__) . "/public" );
}

1;
