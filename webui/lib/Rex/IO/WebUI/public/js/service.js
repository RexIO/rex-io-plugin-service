(function() {

  $(document).ready(function() {

    server_init_hook(function() {

      $("#registered_services, #available_services").sortable({
        connectWith: ".service_list"
      }).disableSelection();


      $("#save_services").button().click(function(event) {
        register_services_to_server(server_id, collect_services());
      });

      $("#run_services").button().click(function(event) {
        $.pnotify({
          text: "not implemented yet.",
          type: "info"
        });
      });

    });

  })

})();

function collect_services() {
  var services = new Array();

  $("#registered_services > li").each(function(idx, itm) {
    services.push($(itm).attr("service_id"));
  });

  return services;
}

function register_services_to_server(server_id, services) {

  $.ajax(
    {
      "type": "POST",
      "url" : "/1.0/service/server/" + server_id,
      "data": JSON.stringify({"data": services})
    }
  ).done(function(data) {
    if(data.ok == true) {
      $.pnotify({
        text: "Services saved",
        type: "info"
      });
    }
    else {
      $.pnotify({
        text: "Error saving services!",
        type: "error"
      });
    }
  });

}
