# Rex.IO Service Plugin

This plugin allows the use of different configuration management systemens with Rex.IO.

The first implementation is the Rex Configuration Management.

## Configuration

First you have to install the plugin. After this you can just add the plugin to the plugin-list of
the Rex.IO Server and Rex.IO WebUI.


```
# File: /etc/rex/io/server.conf and webui.conf

{
  plugins => [
    'Service',
  ],
};
```


## API

List all services:

```
curl -D- -XGET \
  http://user:password@localhost:5000/1.0/service/service
```

List all services of a host:

```
curl -D- -XGET \
  http://user:password@localhost:5000/1.0/service/host/$host_id
```

Adding a new service:

```
curl -D- -XPOST -d '{"name":"basic_os","type_id":1}' \
  http://user:password@localhost:5000/1.0/service/service
```

Remove a service:
Before removing the service, it must be deregistered from all hosts.

```
curl -D- -XDELETE \
  http://user:password@localhost:5000/1.0/service/service/$service_id
```

Add a service to a host:

```
curl -D- -XPOST \
  http://user:password@localhost:5000/1.0/service/service/$service_id/host/$host_id
```

Remove a service from a host:

```
curl -D- -XDELETE \
  http://user:password@localhost:5000/1.0/service/service/$service_id/host/$host_id
```

Remove all services from a host:

```
curl -D- -XDELETE \
  http://user:password@localhost:5000/1.0/service/host/$host_id
```
